package com.yeming.tinyid.util;

/**
 * @author yeming.gao
 * @Description: 返回码接口
 * @date 2020/7/28 17:51
 */
public interface IRespCode {

    String getCode();

    String getDesc();
}
