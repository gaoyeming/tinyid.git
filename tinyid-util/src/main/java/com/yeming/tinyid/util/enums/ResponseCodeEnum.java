package com.yeming.tinyid.util.enums;

import com.yeming.tinyid.util.IRespCode;

/**
 * @author yeming.gao
 * @Description: 返回码枚举
 * @date 2019/6/11 9:47
 */
public enum ResponseCodeEnum implements IRespCode {
    /**
     * 处理成功
     */
    SUCCESS("000", "处理成功"),
    /**
     * 系统异常
     */
    SYS_FAIL("999", "系统异常"),
    /**
     * 业务异常
     */
    BIZ_FAIL("888", "业务处理失败"),
    /**
     * token is wrong
     */
    TOKEN_ERR("100", "token错误"),
    /**
     * 需要去加载nextId
     */
    LOADING("200", "需要去加载nextId"),
    /**
     * 超过maxId 不可用
     */
    OVER("300", "超过maxId 不可用"),

    ;

    private String code;
    private String desc;

    ResponseCodeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
