package com.yeming.tinyid.util.generator;

import java.util.List;

/**
 * @author yeming.gao
 * @Description: 生成id接口
 * @date 2020/7/28 17:24
 */
public interface IdGenerator {
    /**
     * get next id
     * @return
     */
    Long nextId();

    /**
     * get next id batch
     * @param batchSize
     * @return
     */
    List<Long> nextId(Integer batchSize);
}
