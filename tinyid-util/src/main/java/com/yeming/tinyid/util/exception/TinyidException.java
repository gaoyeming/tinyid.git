package com.yeming.tinyid.util.exception;


import com.yeming.tinyid.util.enums.ResponseCodeEnum;

/**
 * @author yeming.gao
 * @Description: 自定义异常
 * @date 2019/6/3 17:51
 */
public class TinyidException extends RuntimeException {
    /**
     * 错误码
     */
    private String code;
    /**
     * 标准错误信息
     */
    private String message;


    public TinyidException(ResponseCodeEnum responseCodeEnum) {
        this.message = responseCodeEnum.getDesc();
        this.code = responseCodeEnum.getCode();
    }

    public TinyidException(String message) {
        this.message = message;
    }


    @Override
    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "TinyidException{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
