package com.yeming.tinyid.util.generator;


import com.yeming.tinyid.util.bo.SegmentId;

/**
 * @author yeming.gao
 * @Description: 根据bizType获取下一个SegmentId对象接口
 * @date 2020/7/28 17:24
 */
public interface ISegmentIdService {

    /**
     * 根据bizType获取下一个SegmentId对象
     * @param bizType 业务类型
     * @return SegmentId
     */
    SegmentId getNextSegmentId(String bizType);

}
