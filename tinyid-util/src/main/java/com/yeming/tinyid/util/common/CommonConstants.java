package com.yeming.tinyid.util.common;

/**
 * @author yeming.gao
 * @Description: 公共常量类
 * @date 2020/7/28 17:24
 */
public class CommonConstants {
    private CommonConstants() {}

    /**
     * 号段预加载因子百分比
     */
    public static final int LOADING_PERCENT = 20;
    /**
     * 重试次数
     */
    public static final int RETRY = 3;

}
