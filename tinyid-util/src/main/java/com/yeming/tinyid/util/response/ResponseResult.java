package com.yeming.tinyid.util.response;

import com.yeming.tinyid.util.IRespCode;
import com.yeming.tinyid.util.enums.ResponseCodeEnum;
import com.yeming.tinyid.util.exception.TinyidException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author yeming.gao
 * @Description: 响应结果
 * @date 2019/11/7 13:52
 */
@Data
public class ResponseResult<T>  implements IRespCode {
    /**
     * 返回结果代码
     */
    private String code;
    /**
     * 具体的错误信息
     */
    private String desc;
    /**
     * 返回结果数据
     */
    private T data;

    public ResponseResult(){}

    public static ResponseResult<?> bizException(TinyidException e){
        ResponseResult<?> responseResult = new ResponseResult<>();
        responseResult.setCode(ResponseCodeEnum.BIZ_FAIL.getCode());
        if(StringUtils.isNotBlank(e.getCode())){
            responseResult.setCode(e.getCode());
        }
        responseResult.setDesc(e.getMessage());
        return responseResult;
    }

    public static ResponseResult<?> sysException(){
        ResponseResult<?> responseResult = new ResponseResult<>();
        responseResult.setCode(ResponseCodeEnum.SYS_FAIL.getCode());
        responseResult.setDesc(ResponseCodeEnum.SYS_FAIL.getDesc());
        return responseResult;
    }

    public ResponseResult(IRespCode iRespCode,T data) {
        this.code = iRespCode.getCode();
        this.desc = iRespCode.getDesc();
        this.data = data;
    }

    public void returnSuccessWithData(T data) {
        this.code = ResponseCodeEnum.SUCCESS.getCode();
        this.desc = ResponseCodeEnum.SUCCESS.getDesc();
        this.data = data;
    }

    public void returnSuccess() {
        this.code = ResponseCodeEnum.SUCCESS.getCode();
        this.desc = ResponseCodeEnum.SUCCESS.getDesc();
    }

    public void returnResp(IRespCode iRespCode) {
        this.code = iRespCode.getCode();
        this.desc = iRespCode.getDesc();
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
