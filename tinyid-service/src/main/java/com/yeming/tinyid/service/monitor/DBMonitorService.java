package com.yeming.tinyid.service.monitor;

import com.yeming.tinyid.dal.mapper.DBMonitorMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yeming.gao
 * @Description: DB 监控服务层
 * @date 2020/7/28 16:13
 */
@Service
public class DBMonitorService {

    @Resource
    private DBMonitorMapper dbMonitorMapper;

    public String selectAll(){
       return dbMonitorMapper.selectMonitor();
    }
}
