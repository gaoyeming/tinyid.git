package com.yeming.tinyid.service.factory;


import com.yeming.tinyid.util.generator.IdGenerator;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yeming.gao
 * @Description: id生成器工厂
 * @date 2020/7/28 17:24
 */
public abstract class AbstractIdGeneratorFactory implements IdGeneratorFactory {

    private static ConcurrentHashMap<String, IdGenerator> generators = new ConcurrentHashMap<>();

    @Override
    public IdGenerator getIdGenerator(String bizType) {
        if (generators.containsKey(bizType)) {
            return generators.get(bizType);
        }
        synchronized (this) {
            if (generators.containsKey(bizType)) {
                return generators.get(bizType);
            }
            IdGenerator idGenerator = createIdGenerator(bizType);
            generators.put(bizType, idGenerator);
            return idGenerator;
        }
    }

    /**
     * 根据bizType创建id生成器
     *
     * @param bizType 业务类型
     * @return IdGenerator
     */
    protected abstract IdGenerator createIdGenerator(String bizType);
}
