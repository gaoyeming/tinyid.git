package com.yeming.tinyid.service.factory.impl;

import com.yeming.tinyid.service.SegmentIdServiceImpl;
import com.yeming.tinyid.service.factory.AbstractIdGeneratorFactory;
import com.yeming.tinyid.util.generator.IdGenerator;
import com.yeming.tinyid.util.generator.impl.CachedIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.Executor;

/**
 * @author yeming.gao
 * @Description: id生成器实现
 * @date 2020/7/29 10:57
 */
@Component
public class IdGeneratorFactoryServer extends AbstractIdGeneratorFactory {

    private static final Logger logger = LoggerFactory.getLogger(IdGeneratorFactoryServer.class);

    @Resource
    private SegmentIdServiceImpl segmentIdServiceImpl;

    @Resource
    private Executor asyncTaskExecutor;

    /**
     * 创建对应的bizType的id生成器
     *
     * @param bizType 业务类型
     * @return IdGenerator
     */
    @Override
    public IdGenerator createIdGenerator(String bizType) {
        logger.info("创建bizType={}的id生成器，并存入自定义缓存", bizType);
        return new CachedIdGenerator(bizType, segmentIdServiceImpl, asyncTaskExecutor);
    }
}