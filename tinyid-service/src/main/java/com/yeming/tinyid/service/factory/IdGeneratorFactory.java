package com.yeming.tinyid.service.factory;


import com.yeming.tinyid.util.generator.IdGenerator;

/**
 * @author yeming.gao
 * @Description: 生成id工厂接口
 * @date 2020/7/28 17:24
 */
public interface IdGeneratorFactory {
    /**
     * 根据bizType创建id生成器
     * @param bizType 业务类型
     * @return IdGenerator
     */
    IdGenerator getIdGenerator(String bizType);
}
