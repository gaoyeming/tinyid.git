package com.yeming.tinyid.application;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

//手动加载自定义配置文件
@PropertySource(value = "common.properties", encoding = "utf-8")
@SpringBootApplication(scanBasePackages = "com.yeming.tinyid")
@ServletComponentScan
@MapperScan("com.yeming.tinyid.dal.mapper")
@EnableScheduling
public class TinyidApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(TinyidApplication.class);

    public static void main(String[] args) {
        //调用SpringApplication启动一个Spring Boot应用
        ApplicationContext app = SpringApplication.run(TinyidApplication.class, args);
        String activeProfile = app.getEnvironment().getActiveProfiles()[0];
        LOGGER.info("当前使用得环境是：{}", activeProfile);
    }

}
