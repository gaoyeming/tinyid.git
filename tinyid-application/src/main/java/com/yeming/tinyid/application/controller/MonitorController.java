package com.yeming.tinyid.application.controller;

import com.yeming.tinyid.service.monitor.DBMonitorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author yeming.gao
 * @Description: 用来监控web应用是否启动成功，和监控DB连接是否成功
 * @date 2020/07/28 16:15
 */
@RestController
public class MonitorController {

    @Resource
    private DBMonitorService dbMonitorService;

    @GetMapping("/monitorWeb")
    public String monitorWeb() {
        return "WEB_OK";
    }

    @GetMapping("/monitorDB")
    public String monitorDB() {
        return dbMonitorService.selectAll();
    }

}
