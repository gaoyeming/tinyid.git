package com.yeming.tinyid.application.exception;

import com.yeming.tinyid.util.exception.TinyidException;
import com.yeming.tinyid.util.response.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author yeming.gao
 * @Description: 全局异常处理类。
 * @date 2020/7/29 10:02
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理自定义异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(TinyidException.class)
    public ResponseResult<?> handlerBizException(TinyidException e) {
        LOGGER.error("业务处理失败：{}", e.getMessage());
        return ResponseResult.bizException(e);
    }

    /**
     * 处理 Exception 异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(Exception.class)
    public ResponseResult<?> handlerSysException(Exception e) {
        LOGGER.error("业务处理异常：", e);
        return ResponseResult.sysException();
    }
}