#### 介绍
Tinyid是用Java开发的一款分布式id生成系统，基于数据库号段算法实现;参照的是滴滴开源的TinyId：[https://github.com/didi/tinyid/wiki/tinyid%E5%8E%9F%E7%90%86%E4%BB%8B%E7%BB%8D](https://github.com/didi/tinyid/wiki/tinyid%E5%8E%9F%E7%90%86%E4%BB%8B%E7%BB%8D)

#### 软件架构
![](./tinyid-application/src/main/resources/tinyid.png)

#### 使用技术

jdk1.8+maven+mysql+h2+http


#### 安装教程

1.  **tinyid-application**模块下的resoureces目录下mybatis-generator-core-1.3.2.jar是mybatis生成中文注解插件；使用附件mybatis-generator-core-install.mvn安装即可
2.  **mvn clean install -Ph2**编译后即可本地运行，使用的是h2内存数据；由**init_db.sql**、**init_data.sql**自动进行初始化；
3.  **mvn clean install -Pmysql**则使用的是mysql数据库，可自行将配置文件**application-mysql.properties**数据库相关配置修改成本地mysql数据库即可
4.  运行成功后，访问：
    * <http://127.0.0.1:8080/tinyid/nextBatchId?bizType=h2&batchSize=10&token=0f673adf80504e2eaa552f5d791b644c>获取指定批量的id
    * <http://127.0.0.1:8080/tinyid/nextId?bizType=h2&token=0f673adf80504e2eaa552f5d791b644c>获取下一个id

## tinyid的原理

* tinyid是基于数据库发号算法实现的，简单来说是数据库中保存了可用的id号段，tinyid会将可用号段加载到内存中，之后生成id会直接内存中产生。

* 可用号段在第一次获取id时加载，如当前号段使用达到一定量时，会异步加载下一可用号段，保证内存中始终有可用号段。

* (如可用号段\~1000被加载到内存，则获取id时，会从1开始递增获取，当使用到一定百分比时，如20%(默认)，即200时，会异步加载下一可用号段到内存，假设新加载的号段是1001\~2000,则此时内存中可用号段为200\~1000,1001\~2000)，当id递增到1000时，当前号段使用完毕，下一号段会替换为当前号段。依次类推。

## 其他说明

* 关于tinyid，是参照滴滴开源的TinyId进行简单改造之后的。具体可以参照滴滴开源的TinyId