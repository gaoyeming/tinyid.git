package com.yeming.tinyid.dal.mapper;

import com.yeming.tinyid.dal.entity.TinyIdInfo;
import org.apache.ibatis.annotations.Param;

public interface TinyIdInfoMapper {
    /**
     * 根据bizType获取db中的tinyId对象
     *
     * @param bizType 业务类型
     * @return TinyIdInfo
     */
    TinyIdInfo queryByBizType(String bizType);

    /**
     * 根据id、oldMaxId、version、bizType更新最新的maxId
     *
     * @param bizType  业务类型
     * @param newMaxId 新生成得maxId=oldMaxId+step
     * @param oldMaxId 老的最大值
     * @param version  版本号
     * @return int
     */
    int updateMaxId(@Param("bizType") String bizType,
                    @Param("newMaxId") Long newMaxId,
                    @Param("oldMaxId") Long oldMaxId,
                    @Param("version") Long version);
}