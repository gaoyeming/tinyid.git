package com.yeming.tinyid.dal.entity;

import java.io.Serializable;
import java.util.Date;

public class TinyIdInfo implements Serializable {
    /**
     * 自增主键
     */
    private Long id;

    /**
     * 业务类型，唯一
     */
    private String bizType;

    /**
     * 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     */
    private Long beginId;

    /**
     * 当前最大id
     */
    private Long maxId;

    /**
     * 步长
     */
    private Integer step;

    /**
     * 每次id增量
     */
    private Integer delta;

    /**
     * 余数
     */
    private Integer remainder;

    /**
     * 版本号
     */
    private Long version;

    /**
     * 创建时间，应用不用维护
     */
    private Date createTime;

    /**
     * 更新时间，应用不用维护
     */
    private Date updateTime;

    /**
     * tiny_id_info
     */
    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     * @return id 自增主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 自增主键
     * @param id 自增主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 业务类型，唯一
     * @return biz_type 业务类型，唯一
     */
    public String getBizType() {
        return bizType;
    }

    /**
     * 业务类型，唯一
     * @param bizType 业务类型，唯一
     */
    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    /**
     * 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     * @return begin_id 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     */
    public Long getBeginId() {
        return beginId;
    }

    /**
     * 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     * @param beginId 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     */
    public void setBeginId(Long beginId) {
        this.beginId = beginId;
    }

    /**
     * 当前最大id
     * @return max_id 当前最大id
     */
    public Long getMaxId() {
        return maxId;
    }

    /**
     * 当前最大id
     * @param maxId 当前最大id
     */
    public void setMaxId(Long maxId) {
        this.maxId = maxId;
    }

    /**
     * 步长
     * @return step 步长
     */
    public Integer getStep() {
        return step;
    }

    /**
     * 步长
     * @param step 步长
     */
    public void setStep(Integer step) {
        this.step = step;
    }

    /**
     * 每次id增量
     * @return delta 每次id增量
     */
    public Integer getDelta() {
        return delta;
    }

    /**
     * 每次id增量
     * @param delta 每次id增量
     */
    public void setDelta(Integer delta) {
        this.delta = delta;
    }

    /**
     * 余数
     * @return remainder 余数
     */
    public Integer getRemainder() {
        return remainder;
    }

    /**
     * 余数
     * @param remainder 余数
     */
    public void setRemainder(Integer remainder) {
        this.remainder = remainder;
    }

    /**
     * 版本号
     * @return version 版本号
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 版本号
     * @param version 版本号
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * 创建时间，应用不用维护
     * @return create_time 创建时间，应用不用维护
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间，应用不用维护
     * @param createTime 创建时间，应用不用维护
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间，应用不用维护
     * @return update_time 更新时间，应用不用维护
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间，应用不用维护
     * @param updateTime 更新时间，应用不用维护
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", bizType=").append(bizType);
        sb.append(", beginId=").append(beginId);
        sb.append(", maxId=").append(maxId);
        sb.append(", step=").append(step);
        sb.append(", delta=").append(delta);
        sb.append(", remainder=").append(remainder);
        sb.append(", version=").append(version);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}