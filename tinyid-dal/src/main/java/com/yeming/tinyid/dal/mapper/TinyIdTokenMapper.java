package com.yeming.tinyid.dal.mapper;

import com.yeming.tinyid.dal.entity.TinyIdToken;
import java.util.List;

public interface TinyIdTokenMapper {

    /**
     * 查询token信息表所有内容
     * @return List<TinyIdToken>
     */
    List<TinyIdToken> selectAll();

}