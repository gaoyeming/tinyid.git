package com.yeming.tinyid.dal.mapper;

/**
 * @author yeming.gao
 * @Description: 数据库探活
 * @date 2020/7/28 16:36
 */
public interface DBMonitorMapper {

    String selectMonitor();
}
