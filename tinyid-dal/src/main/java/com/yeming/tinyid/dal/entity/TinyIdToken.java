package com.yeming.tinyid.dal.entity;

import java.io.Serializable;
import java.util.Date;

public class TinyIdToken implements Serializable {
    /**
     * 自增id
     */
    private Integer id;

    /**
     * token
     */
    private String token;

    /**
     * 此token可访问的业务类型标识
     */
    private String bizType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间，应用不用维护
     */
    private Date createTime;

    /**
     * 更新时间，应用不用维护
     */
    private Date updateTime;

    /**
     * tiny_id_token
     */
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     * @return id 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 自增id
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * token
     * @return token token
     */
    public String getToken() {
        return token;
    }

    /**
     * token
     * @param token token
     */
    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    /**
     * 此token可访问的业务类型标识
     * @return biz_type 此token可访问的业务类型标识
     */
    public String getBizType() {
        return bizType;
    }

    /**
     * 此token可访问的业务类型标识
     * @param bizType 此token可访问的业务类型标识
     */
    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    /**
     * 备注
     * @return remark 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 创建时间，应用不用维护
     * @return create_time 创建时间，应用不用维护
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间，应用不用维护
     * @param createTime 创建时间，应用不用维护
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 更新时间，应用不用维护
     * @return update_time 更新时间，应用不用维护
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间，应用不用维护
     * @param updateTime 更新时间，应用不用维护
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", token=").append(token);
        sb.append(", bizType=").append(bizType);
        sb.append(", remark=").append(remark);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}